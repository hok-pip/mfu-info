import React, { Component } from "react";

import BusList from "./components/BusList";
import BuildingList from "./components/BuildingList";
import Dust from "./components/DustView";

export default class App extends Component {
  render() {
    return (
      <section className="section has-background-success  ">
        <div className="container">
          <div className="content">
            <div className="columns is-multiline">
              <div className="column is-full">
                <Dust />
              </div>{" "}
              <div className="column">
                <BusList />
              </div>
              <div className="column">
                <BuildingList />
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
