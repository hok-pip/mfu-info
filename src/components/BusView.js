import React, { Component } from "react";
import axios from "axios";

var BUS_SRC = "/data/bus.json";

export default class Bus extends Component {
  constructor(props) {
    super(props);
    this.state = {
      busdata: [],
      target: this.props.match.params.id
    };
  }

  componentDidMount() {
    axios.get(BUS_SRC).then(res => {
      this.setState({
        busdata: res.data
      });
    });
  }

  render() {
    let filteredbus = this.state.busdata.filter(bus => {
      return bus.id === this.state.target;
    });
    return (
      <section className="section">
        <div className="container">
          <div className="content">
            <div className="columns is-multiline">
              <div className="column">
                {filteredbus.map(bus => (
                  <div key={bus.id}>
                    <h1 className="title">{bus.title}</h1>
                    <a
                      target="_blank"
                      href={`https://www.google.com/maps/@${bus.lat},${bus.long}`}
                    >
                      <i class="fas fa-location-arrow"></i> Google Maps
                    </a>
                    <h2 className="subtitle">{bus.description}</h2>
                    <img src={bus.image} alt={bus.title}></img>

                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
