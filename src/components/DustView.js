import React, { Component } from "react";
import axios from "axios";
var DUST_SRC =
  "https://www.cmuccdc.org/api2/dustboy/first/20.0481382/99.89518249999999";
// var DUST_SRC = "/data/dust.json"
export default class dust extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dustdata: []
    };
  }

  componentDidMount() {
    axios.get(DUST_SRC).then(res => {
      this.setState({ dustdata: res.data });
    });
  }
  render() {
    function getaqicolor(AQI) {
      if (AQI > 0 && AQI <= 25) {
        return "blue";
      } else if (AQI >= 26 && AQI <= 50) {
        return "green";
      } else if (AQI >= 51 && AQI <= 100) {
        return "yellow";
      } else if (AQI >= 101 && AQI <= 200) {
        return "orangered";
      } else if (AQI >= 201 && AQI <= 300) {
        return "purple";
      } else if (AQI > 300) {
        return "maroon";
      } else return "black";
    }
    function getaqistyle(AQI) {
      if (AQI > 0 && AQI <= 25) {
        return "is-info";
      } else if (AQI >= 26 && AQI <= 50) {
        return "is-success";
      } else if (AQI >= 51 && AQI <= 100) {
        return "is-warning";
      } else if (AQI >= 101 && AQI <= 200) {
        return "is-danger";
      } else if (AQI >= 201 && AQI <= 300) {
        return "is-danger";
      } else if (AQI > 300) {
        return "is-danger";
      } else return "is-dark";
    }
    return (
      <div className="box">
        <section className="section">
          <div className="container">
            <div className="columns is-multiline">
              <div className="column is-full has-text-centered">
                <h1 className="title is-1">Weather &amp; Dust</h1>
                <h2 className="subtitle is-4">
                  {this.state.dustdata.dustboy_name}
                </h2>
              </div>
              <div className="column is-full has-text-centered">
                <h3 className="title is-3">
                  <i class="fas fa-thermometer-half"></i>{" "}
                  {this.state.dustdata.temp}&deg;C
                  {"  "}
                  <i class="fas fa-tint"></i> {this.state.dustdata.humid}&#37;
                </h3>
                <h4 className="subtitle is-5">
                  PM2.5: {this.state.dustdata.pm25} &mu;g/m&sup3;
                  {" | "}
                  PM10: {this.state.dustdata.pm10} &mu;g/m&sup3;
                </h4>
              </div>
              
              <div className="column is-half">
                <div className="card">
                  <header class="card-header has-text-white-ter">
                    <p class="card-header-title">US AQI</p>
                  </header>
                  <div
                    className="card-content"
                    style={{
                      backgroundColor: `rgb(${this.state.dustdata.us_color})`
                    }}
                  >
                    <div className="content">
                      <h1 className="title is-1 has-text-centered has-text-white-ter">
                        {this.state.dustdata.us_aqi}
                      </h1>
                      <article
                        className={`message ${getaqistyle(
                          this.state.dustdata.us_aqi
                        )}`}
                      >
                        <div className="message-header">
                          <p>{this.state.dustdata.us_title}</p>
                        </div>
                        <div className="message-body">
                          {this.state.dustdata.us_caption}
                        </div>
                      </article>
                    </div>
                  </div>
                  <div className="card-footer"></div>
                </div>
              </div>
              <div className="column is-half">
                <div className="card">
                  <header class="card-header has-text-white-ter">
                    <p class="card-header-title">TH AQI</p>
                  </header>
                  <div
                    className="card-content"
                    style={{
                      backgroundColor: `rgb(${this.state.dustdata.th_color})`
                    }}
                  >
                    <div className="content">
                      <h1 className="title is-1 has-text-centered has-text-white-ter">
                        {this.state.dustdata.th_aqi}
                      </h1>
                      <article
                        className={`message ${getaqistyle(
                          this.state.dustdata.th_aqi
                        )}`}
                      >
                        <div className="message-header">
                          <p>{this.state.dustdata.th_title}</p>
                        </div>
                        <div className="message-body">
                          {this.state.dustdata.th_caption}
                        </div>
                      </article>
                    </div>
                  </div>
                  <div className="card-footer"></div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
