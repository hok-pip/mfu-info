import React, { Component } from "react";
import { Link } from "react-router-dom";

import axios from "axios";

var BUILDINGS_SRC = "/data/buildings.json";

export default class buildinglist extends Component {
  constructor(props) {
    super(props);
    this.state = {
      buildingsdata: []
    };
  }

  componentDidMount() {
    axios.get(BUILDINGS_SRC).then(res => {
      this.setState({ buildingsdata: res.data });
    });
  }
  render() {
    return (
      <div className="box">
        <h1 className="title">Buildings</h1>
        {this.state.buildingsdata.map(building => (
          <ul>
            <Link to={`building/${building.id}`}>
              <li key={building.id}>{building.title}</li>
            </Link>
          </ul>
        ))}
      </div>
    );
  }
}
