import React, { Component } from "react";

import axios from "axios";

var BUILDING_SRC = "/data/buildings.json";

export default class Bus extends Component {
  constructor(props) {
    super(props);
    this.state = {
      buildingdata: [],
      target: this.props.match.params.id
    };
  }

  componentDidMount() {
    axios.get(BUILDING_SRC).then(res => {
      this.setState({
        buildingdata: res.data
      });
    });
  }

  render() {
    let filteredbuilding = this.state.buildingdata.filter(building => {
      return building.id === this.state.target;
    });
    return (
      <section className="section">
        <div className="container">
          <div className="content">
            <div className="columns is-multiline">
              <div className="column">
                {filteredbuilding.map(building => (
                  <div key={building.id}>
                    <h1 className="title">{building.title}</h1>{" "}
                    <a
                      target="_blank"
                      href={`https://www.google.com/maps/@${building.lat},${building.long}`}
                    >
                      <i class="fas fa-location-arrow"></i> Google Maps
                    </a>
                    <h2 className="subtitle">{building.description}</h2>
                    <img src={building.image} alt={building.title}></img>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
