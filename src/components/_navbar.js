import React from "react";
import { Link } from "react-router-dom";
import logo from "../mfu.png";

const Navbar = () => {
  return (
    <nav className="navbar is-light">
      <div className="container">
        <div className="navbar-brand">
          <Link to={`/`} className="navbar-item">
            <img src={logo} alt="logo"></img>
          </Link>
          <div className="navbar-item">
            <Link to={`/`} className="navbar-item">
              MFU info by Hok Pip
            </Link>
          </div>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
