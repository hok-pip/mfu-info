import React, { Component } from "react";
import { Link } from "react-router-dom";

import axios from "axios";
var BUS_SRC = "/data/bus.json";

export default class buslist extends Component {
  constructor(props) {
    super(props);
    this.state = {
      busdata: []
    };
  }

  componentDidMount() {
    axios.get(BUS_SRC).then(res => {
      this.setState({ busdata: res.data });
    });
  }
  render() {
    return (
      <div className="box"> 
        <h1 className="title">Bus stops</h1>
        {this.state.busdata.map(bus => (
          <ul>
            <Link to={`bus/${bus.id}`}>
              <li key={bus.id}>{bus.title}</li>
            </Link>
          </ul>
        ))}
      </div>
    );
  }
}
