import React from "react";
import ReactDOM from "react-dom";
import { HashRouter, Route, Switch } from "react-router-dom";

import App from "./App";
import Bus from "./components/BusView";
import Building from "./components/BuildingView";
import Navbar from "./components/_navbar";

import "./index.css";
import * as serviceWorker from "./serviceWorker";

ReactDOM.render(
  <HashRouter>
    <Navbar />
    <Switch>
      <Route exact path="/" component={App} />
      <Route path="/bus/:id" component={Bus} />
      <Route path="/building/:id" component={Building} />
    </Switch>
  </HashRouter>,
  document.getElementById("root")
);

serviceWorker.unregister();
